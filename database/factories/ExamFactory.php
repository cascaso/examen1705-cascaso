<?php

use Faker\Generator as Faker;

$factory->define(Model::class, function (Faker $faker) {
    return [
        //
    ];
});


$factory->define(App\Exam::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'date' => $faker->date($format = 'Y-m-d', $min = '2017-09-01', $max = '2018-06-01'),
        'module_id' => rand(1, 4),
        'user_id' => rand(1, 4),
    ];
});
