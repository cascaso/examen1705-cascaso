@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card card-default">
        <div class="card-header">Dashboard</div>

        <div class="card-body">
          <table class="table">
            <thead>
              <tr>
                <th>Enunciado</th>
                <th>A</th>
                <th>B</th>
                <th>C</th>
                <th>D</th>
                <th>Solucion</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($questions as $question)
              <tr>
                <td>  {{ $question->text }} </td>
                <td @if($question->a === $question->answer) class='bg-success' @endif>  {{ $question->a }} </td>
                <td @if($question->b === $question->answer) class='bg-success' @endif>  {{ $question->b }} </td>
                <td @if($question->c === $question->answer) class='bg-success' @endif>  {{ $question->c }} </td>
                <td @if($question->d === $question->answer) class='bg-success' @endif>  {{ $question->d }} </td>
                <td>  {{ $question->answer }} </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
