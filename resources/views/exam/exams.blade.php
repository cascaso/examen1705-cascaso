@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card card-default">
        <div class="card-header">Dashboard</div>

        <div class="card-body">
          <table class="table">
            <thead>
              <tr>
                <th>Title</th>
                <th>Date</th>
                <th>Materia</th>
                <th>Usuario</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($exams as $exam)
              <tr>
                <td>  {{ $exam->title }} </td>
                <td>  {{ $exam->date }} </td>
                <td>  {{ $exam->module->name }} </td>
                <td>  {{ $exam->user->name }} </td>
                <td> <a class="btn btn-primary" {{--href="{{ route('exams.show', $exam->id) }}"--}}> Recordar </a></td>
                <td> <a class="btn btn-danger" {{--href="{{ route('exams.edit', $exam->id) }}"--}}> Borrar </a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
          {{ $exams->links() }}
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
