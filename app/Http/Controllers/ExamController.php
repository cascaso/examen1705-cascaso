<?php

namespace App\Http\Controllers;

use App\Exam;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ExamController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {

    if (Auth::check()) {
      $exams = Exam::with('user', 'module')->paginate(10);

      return view('exam.exams',['exams' => $exams]);
    }

    return view('auth.login');

  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    //
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    //
  }

  /**
  * Display the specified resource.
  *
  * @param  \App\Exam  $exam
  * @return \Illuminate\Http\Response
  */
  public function show(Exam $exam)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  \App\Exam  $exam
  * @return \Illuminate\Http\Response
  */
  public function edit(Exam $exam)
  {
    //
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  \App\Exam  $exam
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, Exam $exam)
  {
    //
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  \App\Exam  $exam
  * @return \Illuminate\Http\Response
  */
  public function destroy(Exam $exam)
  {
    //
  }
}
