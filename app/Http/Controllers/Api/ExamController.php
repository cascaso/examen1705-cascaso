<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Exam;

class ExamController extends Controller
{
  public function index()
  {
    return Exam::all();
  }

  public function show($id)
  {


    return Exam::with('module')->find($id);
  }

  public function store(Request $request)
  {
    return Exam::create($request->all());
  }

  public function update(Request $request, $id)
  {
    $Exam = Exam::findOrFail($id);
    $Exam->update($request->all());

    return $Exam;
  }

  public function destroy(Request $request, $id)
  {
    $Exam = Exam::findOrFail($id);
    $Exam->delete();

    return 204;
  }
}
